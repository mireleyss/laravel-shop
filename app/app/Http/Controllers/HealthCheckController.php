<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HealthCheckController extends Controller
{
    public function index(): JsonResponse
    {
        return new JsonResponse([
            'status' => 'working',
        ]);
    }
}
